#define pc(x) putchar_unlocked(x)
#define gc(x) getchar_unlocked(x)

void scan_int(int &x) {
    register int c = gc();
    x = 0;
    int neg = 0;
    for(; ((c < 48 || c > 57) && c != '-'); c = gc());
    if(c == '-') {
        neg = 1;
        c = gc();
    }
    for(; c > 47 && c < 58; c = gc()) {
        x = (x << 1) + (x << 3) + c - 48;
    }
    if(neg)
        x = -x;
}

inline void print_int(int n) {
    int N = n, rev, count = 0;
    rev = N;
    if(N == 0) {
        pc('0');
        return;
    }
    while((rev % 10) == 0) {
        count++; rev /= 10;
    }
    rev = 0;
    while(N != 0) {
        rev = (rev << 3) + (rev<<1) + N % 10;
        N /= 10;
    }
    while(rev != 0) {
        pc(rev % 10 + '0');
        rev /= 10;
    }
    while(count--)
        pc('0');
}

inline void print_string(char *string) {
    while(*string != '\0') {
        pc(*string);
        string++;
    }
}