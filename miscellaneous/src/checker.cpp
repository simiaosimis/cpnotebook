while true
do
    python3 gen.py > in
    cat in | ./fast_solution > a
    cat in | ./slow_but_correct_solution > b
    DIFF=$(diff a b)
    if [ "$DIFF" != "" ]
    then
        echo "BOOM! FOUND DIFFERENCE!"
        break
    fi
    echo "OK!"
done
