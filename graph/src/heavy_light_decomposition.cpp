#include <bits/stdc++.h>

#define debug(x) cout << #x << " = " << x << endl
#define fori(i, ini, lim) for(int i = int(ini); i < int(lim); i++)
#define ford(i, ini, lim) for(int i = int(ini); i >= int(lim); i--)

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;

const int MAX = 1e4 + 5;
const int LOGMAX = 31 - __builtin_clz(MAX) + 3;
int N;
int P[LOGMAX][MAX], L[MAX];
vector<int> adj[MAX], costs[MAX], indexx[MAX];
int st[4 * MAX + 5];
int chain[MAX], chain_head[MAX], other_end[MAX], sz[MAX];
int base[MAX], base_pos[MAX];
int chain_cnt, node_cnt;

void build(int p, int l, int r) {
    if(l == r) {
        st[p] = base[l];
        return;
    }
    build(p * 2, l, (l + r) / 2);
    build(p * 2 + 1, (l + r) / 2 + 1, r);
    st[p] = max(st[p * 2], st[p * 2 + 1]);
}

void update(int p, int l, int r, int idx, int val) {
    if(l == r && l == idx) {
        st[p] = val;
        return;
    }
    if(l > idx || idx > r) {
        return;
    }
    update(p * 2, l, (l + r) / 2, idx, val);
    update(p * 2 + 1, (l + r) / 2 + 1, r, idx, val);
    st[p] = max(st[p * 2], st[p * 2 + 1]);
}

int query(int p, int l, int r, int i, int j) {
    if(j >= r && l >= i) {
        return st[p];
    }
    if(i > r || l > j) {
        return -(1 << 30);
    }
    return max(query(2 * p, l, (l + r) / 2, i, j), query(2 * p + 1, (l + r) / 2 + 1, r, i, j));
}


int dfs(int source, int father) {
    sz[source] = 1;
    fori(i, 0, adj[source].size()) {
        int each = adj[source][i];
        if(each != father) {
            P[0][each] = source;
            L[each] = L[source] + 1;
            other_end[indexx[source][i]] = each;
            sz[source] += dfs(each, source);
        }
    }
    return sz[source];
}

int lca(int u, int v) {
    if(L[u] > L[v]) {
        swap(u, v);
    }

    int lg = 31 - __builtin_clz(L[v]);
    ford(i, lg, 0) {
        if(L[v] - (1 << i) >= L[u]) {
            v = P[i][v];
        }
    }

    if(u == v) {
        return u;
    }

    lg = 31 - __builtin_clz(L[v]);
    ford(i, lg, 0) {
        if(P[i][u] != P[i][v]) {
            u = P[i][u];
            v = P[i][v];
        }
    }
    return P[0][u];
}

void hld(int source, int father, int cost) {
    if(chain_head[chain_cnt] == -1) {
        chain_head[chain_cnt] = source;
    }
    chain[source] = chain_cnt;
    base_pos[source] = node_cnt;
    base[node_cnt++] = cost;

    int bc = -1, bc_cost = -1;
    fori(i, 0, adj[source].size()) {
        int each = adj[source][i];
        if(each != father) {
            if(bc == -1 || sz[each] > sz[bc]) {
                bc = each;
                bc_cost = costs[source][i];
            }
        }
    }
    
    if(bc != -1) {
        hld(bc, source, bc_cost);
    }

    fori(i, 0, adj[source].size()) {
        int each = adj[source][i];
        if(bc != each && each != father) {
            chain_cnt++;
            hld(each, source, costs[source][i]);
        }
    }
}

int query_up(int u, int v) {
    if(u == v) {
        return 0;
    }
    int u_chain = chain[u], v_chain = chain[v], ans = -1;
    while(1) {
        u_chain = chain[u];
        if(u_chain == v_chain) {
            if(u != v) {
                ans = max(ans, query(1, 1, node_cnt, base_pos[v] + 1, base_pos[u]));
            }
            break;
        }
        else {
            ans = max(ans, query(1, 1, node_cnt, base_pos[chain_head[u_chain]], base_pos[u])); 
            u = chain_head[u_chain];
            u = P[0][u];
        }
    }
    return ans;
}

// THIS CODE WILL FIND THE MAX EDGE VALUE IN THE PATH FROM A TO B.

int main() {
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d", &N);
        fori(i, 1, N + 1) {
            adj[i].clear();
            costs[i].clear();
            indexx[i].clear();
        }

        fori(i, 0, N - 1) {
            int a, b, c;
            scanf("%d %d %d", &a, &b, &c);
            adj[a].push_back(b);
            adj[b].push_back(a);
            costs[a].push_back(c);
            costs[b].push_back(c);
            indexx[a].push_back(i);
            indexx[b].push_back(i);
        }

        memset(chain_head, -1, sizeof chain_head);
        memset(P, -1, sizeof P);
        chain_cnt = 1;
        node_cnt = 1;
        dfs(1, -1);
        hld(1, -1, -1);
        node_cnt--;
        build(1, 1, node_cnt);

        fori(i, 1, LOGMAX) {
            fori(j, 1, N + 1) {
                if(P[i - 1][j] != -1) {
                    P[i][j] = P[i - 1][P[i - 1][j]];
                }
            }
        }

        char str[10];
        while(scanf("%s", str), str[0] != 'D') {
            if(str[0] == 'Q') {
                int a, b;
                scanf("%d %d", &a, &b);
                int lc = lca(a, b);
                int q1 = query_up(a, lc);
                int q2 = query_up(b, lc);
                printf("%d\n", max(q1, q2));
            }
            else {
                int e, ci;
                scanf("%d %d", &e, &ci);
                int node = other_end[e - 1];
                update(1, 1, node_cnt, base_pos[node], ci);
            }
        }
    }

    return 0;
}

