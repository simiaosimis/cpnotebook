const int MAX = 1e5 + 5;
int N, M;
vector<int> adj[MAX];
vector<ii> queries[MAX];
int sz[MAX], ans[MAX], value[MAX];
bool big[MAX];

int calc_size(int s, int f = -1) {
    sz[s] = 1;
    for(auto &each : adj[s]) {
        if(each != f) {
            sz[s] += calc_size(each, s);
        }
    }
    return sz[s];
}

void add(int s, int f) {
    // add something
    for(auto &each : adj[s]) {
        if(each != f && !big[each]) {
            add(each, s);
        }
    }
}

void remove(int s, int f) {
    // remove something
    for(auto &each : adj[s]) {
        if(each != f && !big[each]) {
            remove(each, s);
        }
    }
}

void dfs(int s, int f, int keep) {
    int mx = -1, big_child = -1;
    for(auto &each : adj[s]) {
        if(each != f && sz[each] > mx) {
            mx = sz[each];
            big_child = each;
        }
    }
    for(auto &each : adj[s]) {
        if(each != f && each != big_child) {
            dfs(each, s, 0);
        }
    }
    if(big_child != -1) {
        dfs(big_child, s, 1);
        big[big_child] = true;
    }
    add(s, f);
    for(auto &q : queries[s]) { // answer queries here
        //ans[q.second] = something;
    }
    if(big_child != -1) {
        big[big_child] = false;
    }
    if(keep == 0) {
        remove(s, f);
    }
}

int main() {
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d %d", &N, &M);
        fori(i, 1, N + 1) {
            adj[i].clear();
            queries[i].clear();
            scanf("%d", &value[i]);
        }
        fori(i, 0, N - 1) {
            int a, b;
            scanf("%d %d", &a, &b);
            adj[a].push_back(b);
            adj[b].push_back(a);
        }
        fori(i, 0, M) {
            int x, k;
            scanf("%d %d", &x, &k);
            queries[x].emplace_back(k, i);
        }
        
        calc_size(1);
        CNT = 1;
        memset(big, false, sizeof big);
        dfs(1, -1, 1);
        fori(i, 0, M) {
            printf("%d ", ans[i]);
        }
        printf("\n");
    }

    return 0;
}
