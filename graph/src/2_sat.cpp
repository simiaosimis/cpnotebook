inline int eval(int x) {
    return 2 * x;
}

inline void add_edge(int u, int v) {
    adj[u ^ 1].push_back(v);
    adj[v ^ 1].push_back(u);
    rvr[v].push_back(u ^ 1);
    rvr[u].push_back(v ^ 1);
}

fori(i, 0, segments.size()) {
    int e = eval(i);
    if(comp[e] > comp[e ^ 1]) {
        ans[i] = 1;
    }
}