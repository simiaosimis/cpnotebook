const int MAX = 1e5;
constexpr int LOGMAX = sizeof(int) * 8 - __builtin_clz(MAX) - 1;

int N;
vector<ii> adj[MAX + 5];
ll dis[MAX + 5];
int L[MAX + 5], F[MAX + 5], S[MAX + 5];
int P[MAX + 5][LOGMAX + 5];

void dfs(int s) {
    for(auto each : adj[s]) {
        dis[each.first] = dis[s] + each.second;
        L[each.first] = L[s] + 1;
        dfs(each.first);
    }
}

int lca(int a, int b) {
    if(L[b] > L[a]) {
        swap(a, b);
    }
    int lg = sizeof(int) * 8 - __builtin_clz(L[a]) - 1;
    for(int i = lg; i >= 0; i--) {
        if(L[a] - (1 << i) >= L[b]) {
            a = P[a][i];
        } 
    }

    if(a == b) {
        return a;
    }

    lg = sizeof(int) * 8 - __builtin_clz(L[a]) - 1;
    for(int i = lg; i >= 0; i--) {
        if(P[a][i] != P[b][i]) {
            a = P[a][i], b = P[b][i];
        }
    }
    return F[a];
}

int main() {
    while(scanf("%d", &N), N) {
        for(int i = 0; i < N; i++) {
            adj[i].clear();
        }

        memset(F, 0, sizeof F);
        F[0] = -1;
        for(int i = 1; i <= N - 1; i++) {
            adj[i].clear();
            int a, l;
            scanf("%d %d", &a, &l);
            adj[a].emplace_back(i, l);
            F[i] = a;
        }

        memset(P, -1, sizeof P);
        for(int i = 0; i < N; i++) {
            P[i][0] = F[i];
        }
        
        for(int j = 1; j < LOGMAX; j++) {
            for(int i = 0; i < N; i++) {
                if(P[i][j - 1] != -1) {
                    P[i][j] = P[P[i][j - 1]][j - 1];
                }   
            }
        }
        memset(L, 0, sizeof L);
        memset(dis, 0, sizeof dis);
        dfs(0);
    }

    return 0;
}
