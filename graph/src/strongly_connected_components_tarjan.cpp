#include <bits/stdc++.h>

using namespace std;

const int N = 2345;

vector< vector<int> > adj;
int V, E;

int num[N], low[N];
int dfs_t;
bool on[N];

void scc(int u, int &numScc, stack<int> &dfs_s) {
    if(numScc > 1)
        return;
    num[u] = low[u] = ++dfs_t;
    dfs_s.push(u);
    on[u] = true;

    for(int v : adj[u]) {
        if(num[v] == -1)
            scc(v, numScc, dfs_s);
        if(on[v])
            low[u] = min(low[u], low[v]);
    }

    if(num[u] == low[u]) {
        ++numScc;
        
        while(true) {
                int v = dfs_s.top(); // all vertices v are in the same SCC
            dfs_s.pop();
            on[v] = false;
            if(u == v)
                break;
        }
    }
}

int main() {

    while(scanf("%d %d", &V, &E), V | E) {
        adj.clear();
        adj.resize(V + 5);

        for(int i = 0; i < E; ++i) {
            int u, v, p;
            scanf("%d %d %d", &u, &v, &p);
            adj[u].push_back(v);            // G is directed
        }

        memset(on, 0, sizeof on);
        memset(num, -1, sizeof num);
        memset(low, -1, sizeof low);

        dfs_t = 0;          // dfs time
        int numScc = 0;     // number of strongly connected components
        stack<int> dfs_s;   // stack of vertices

        for(int u = 1; u <= V; ++u)
            if(num[u] == -1)
                scc(u, numScc, dfs_s);

        printf("%d\n", numScc);
    }

    return 0;
}
