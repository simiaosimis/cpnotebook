typedef pair<int, int> ii;
vector<ii> adj[MAX+5];
int dist[MAX+5];
 
void dij(int s){
    dist[s] = 0;
    priority_queue<ii, vector<ii>, greater<ii> > pq;
    pq.push(ii(0,s));
    while(!pq.empty()){
        ii cur = pq.top(); pq.pop();
        int u = cur.second;
        int w = cur.first;
        if(w > dist[u]) continue;
        fori(i,0,adj[u].size()){
            ii cadj = adj[u][i];
            int v = cadj.first;
            int nw = cadj.second;
            if(dist[u] + nw < dist[v]){
                dist[v] = dist[u] + nw;
                pq.push(ii(dist[v], v));
            }
        }
    }
}