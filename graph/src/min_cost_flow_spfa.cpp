struct edge {
	int u, v;
	ll cap, cost;
 
	edge(int _u, int _v, ll _cap, ll _cost) {
		u = _u; v = _v; cap = _cap; cost = _cost;
	}
};
 
struct min_cost_flow {
public:
	int n, s, t;
	ll flow, cost;
	vector< vector<int> > graph;
	vector<edge> e;
	vector<ll> dist;
	vector<int> parent;
 
	min_cost_flow(int _n) {
		// 0-based indexing
		n = _n;
		graph.assign(n, vector<int> ());
	}
 
	void add_edge(int u, int v, ll cap, ll _cost, bool directed = true) {
		graph[u].push_back(e.size());
		e.push_back(edge(u, v, cap, _cost));
 
		graph[v].push_back(e.size());
		e.push_back(edge(v, u, 0, -_cost));
 
		if(!directed) {
			add_edge(v, u, cap, cost, true);
		}
	}
 
	pair<ll, ll> get_min_cost_flow(int _s, int _t) {
		s = _s; t = _t;
		flow = 0, cost = 0;
		while(SPFA()) {
			flow += send_flow(t, 1LL << 62);
		}
		return make_pair(flow, cost);
	}
 
private:
	bool SPFA() {
		parent.assign(n, -1);
		dist.assign(n, 1LL << 62);
		dist[s] = 0;
		vector<int> queuetime(n, 0);
		queuetime[s] = 1;
		vector<bool> inqueue(n, 0);
		inqueue[s] = true;
		queue<int> q;
		q.push(s);
		bool negativecycle = false;
 
		while(!q.empty() && !negativecycle) {
			int u = q.front(); q.pop(); inqueue[u] = false;
 
			for(int i = 0; i < (int) graph[u].size(); i++) {
				int eIdx = graph[u][i];
				int v = e[eIdx].v; ll w = e[eIdx].cost, cap = e[eIdx].cap;
 
				if(dist[u] + w < dist[v] && cap > 0) {
					dist[v] = dist[u] + w;
					parent[v] = eIdx;
 
					if(!inqueue[v]) {
						q.push(v);
						queuetime[v]++;
						inqueue[v] = true;
 
						if(queuetime[v] == n+2) {
							negativecycle = true;
							break;
						}
					}
				}
			}
		}
 
		return dist[t] != (1LL << 62);
	}
 
	ll send_flow(int v, ll cur_flow) {
		if(parent[v] == -1) {
			return cur_flow;
		}

		int eIdx = parent[v];
		int u = e[eIdx].u; ll w = e[eIdx].cost;
 
		ll f = send_flow(u, min(cur_flow, e[eIdx].cap));
 
		cost += f * w;
		e[eIdx].cap -= f;
		e[eIdx ^ 1].cap += f;
 
		return f;
	}
};