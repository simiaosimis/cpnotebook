const int MAX = 2e5 + 5;
vector<int> adj[MAX], tree[MAX];
set<ii> bridges;
map<ii, int> app;
int cc[MAX], bcc[MAX], low[MAX], num[MAX], parent[MAX];
int dfs_cnt, cc_cnt, bcc_cnt;
int n, m, q;

void find_bridges(int source) {
    cc[source] = cc_cnt;
    low[source] = num[source] = dfs_cnt++;
    for(auto &each : adj[source]) {
        if(num[each] == -1) {
            parent[each] = source;
            find_bridges(each);
            if(app[ii(min(each, source), max(each, source))] == 1 && low[each] > num[source]) {
                bridges.emplace(min(each, source), max(each, source));
            }
            low[source] = min(low[source], low[each]);
        }
        else if(parent[source] != each) {
            low[source] = min(low[source], num[each]);
        }
    }
}

bool visited[MAX];
void unite(int source) {
    visited[source] = true;
    bcc[source] = bcc_cnt;
    for(auto &each : adj[source]) {
        if(bridges.count(ii(min(each, source), max(each, source)))) {
            continue;
        }
        if(!visited[each]) {
            unite(each);
        }
    }
}

void build_bridge_tree() {
    memset(parent, -1, sizeof parent);
    memset(num, -1, sizeof num);
    cc_cnt = 1;
    fori(i, 1, n + 1) {
        if(num[i] == -1) {
            dfs_cnt = 1;
            find_bridges(i);
            cc_cnt++;
        }
    }

    bcc_cnt = 1;
    memset(visited, false, sizeof visited);
    fori(i, 1, n + 1) {
        if(!visited[i]) {
            unite(i);
            bcc_cnt++;
        }
    }
    bcc_cnt--;

    for(auto &each : bridges) {
        tree[bcc[each.first]].push_back(bcc[each.second]);
        tree[bcc[each.second]].push_back(bcc[each.first]);
    }
}

int main() {
    scanf("%d %d %d", &n, &m, &q);
    fori(i, 0, m) {
        int u, v;
        scanf("%d %d", &u, &v);
        adj[u].push_back(v);
        adj[v].push_back(u);
        app[ii(min(u, v), max(u, v))]++;
    }
    
    build_bridge_tree();

    return 0;
}

