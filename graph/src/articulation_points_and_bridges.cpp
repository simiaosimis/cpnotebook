#include <bits/stdc++.h>

#define debug(x) cout << "> " << #x << " = " << x << endl;
#define debugat(arr, at) cout << "> " << #arr << "[" << at << "] = " << arr[at] << endl;

using namespace std;

const int N = 1234;
vector< vector<int> > adj;
int V, E;
int num[N], low[N], parent[N];
bool articulation_vertex[N];

void dfs(int u, int &dfs_t) { // u = current vertexm, dfs_t = dfs time
    num[u] = low[u] = ++dfs_t;
    for(int v : adj[u]) {
        if(num[v] == -1) {
            parent[v] = u;
            dfs(v, dfs_t);
        }
        if(num[u] < low[v]) { // (u, v) is a bridge
             printf("edge (%d, %d) is a bridge\n", u, v);
        }
        if(num[u] <= low[v]) { // u is an articulation_vertex
            articulation_vertex[u] = true;
        }
        if(v != parent[u])
            low[u] = min(low[u], low[v]);
    }
}

int main() {
    while(scanf("%d %d", &V, &E) == 2) {
        adj.clear();
        adj.resize(V + 5);

        for(int i = 0; i < E; ++i) {
            int u, v;
            scanf("%d %d", &u, &v);
            adj[u].push_back(v);
            adj[v].push_back(u);
        }
        memset(parent, 0, sizeof parent);
        memset(num, -1, sizeof num);
        memset(low, -1, sizeof low);
        int ans = 0, dfs_t = 0;
        dfs(1, dfs_t);
        printf("%d\n", ans);
    } 

    return 0;
}
