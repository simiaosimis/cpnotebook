// MVC = minimum subset nodes of nodes that cover all edges of the graph
// Minimum vertex cover in a bipartite graph equals to its maximum matching.
// The maximum independent set is the set of nodes of greatest size s.t. no node is connected by an edge

const int MAX = 2e3 + 5;
const int MAX_VAL = 2e5;
const int INF = 0x3F3F3F3F;
int v[MAX], match[MAX], back[MAX];
int n;
bool prime[MAX_VAL + 5], visited[MAX], in_cover[MAX];
vector<int> adj[MAX], odds, evens;

void sieve() {
    memset(prime, true, sizeof prime);
    prime[1] = false;
    for(int i = 4; i <= MAX_VAL; i += 2) {
        prime[i] = false;
    }
    for(int i = 3; i <= MAX_VAL; i += 2) {
        if(prime[i]) {
            for(int j = 2 * i; j <= MAX_VAL; j += i) {
                prime[j] = false;
            }
        }
    }
}

int aug(int source) {
    if(visited[source]) {
        return 0;
    }
    visited[source] = true;
    for(auto &each : adj[source]) {
        if(match[each] == -1 || aug(match[each])) {
            match[each] = source;
            back[source] = each;
            return 1;
        }
    }
    return 0;
}
int f() {
    memset(match, -1, sizeof match);
    memset(back, -1, sizeof back);
    int flow = 0;
    for(auto &each : odds) {
        memset(visited, false, sizeof visited);
        flow += aug(each);
    }
    return flow;
}
void roll(int node) {
    if(in_cover[node]) {
        return;
    }
    for(auto &each : adj[node]) {
        if(!in_cover[each]) {
            in_cover[match[each]] = false;
            in_cover[each] = true;
            roll(match[each]);
        }
    }
}
int main() {
    sieve();
    scanf("%d", &n);
    fori(i, 1, n + 1) {
        scanf("%d", v + i);
    }
    fori(i, 1, n + 1) {
        if(v[i] & 1) {
            fori(j, 1, n + 1) {
                if(i != j) {
                    if(prime[v[i] + v[j]]) {
                        adj[i].push_back(j);
                    }
                }
            }
            odds.push_back(i);
        }
        else {
            evens.push_back(i);
        }
    }
    int max_matching = f();
    for(auto &odd : odds) {
        in_cover[odd] = back[odd] != -1;
    }
    for(auto &odd : odds) {
        roll(odd);
    }
    
    vector<int> nodes;
    for(auto &odd : odds) {
        if(in_cover[odd]) {
            nodes.push_back(v[odd]);
        }
    }
    for(auto &even : evens) {
        if(in_cover[even]) {
            nodes.push_back(v[even]);
        }
    }

    printf("%d\n", max_matching);
    for(auto &node : nodes) {
        printf("%d ", node);
    }
    printf("\n");

    return 0;
}

