const int MAX = 1e5;
vector<ii> adj[MAX + 5];
int L[MAX + 5], P[MAX + 5], S[MAX + 5];
ll dis[MAX + 5];
int sqr;

void dfs(int s) {
    if(L[s] < sqr) {
        S[s] = 1;
    }
    else {
        if(!(L[s] % sqr)) {
            S[s] = P[s];
        }
        else {
            S[s] = S[P[s]];
        }
    }

    for(auto each : adj[s]) {
        dis[each.first] = dis[s] + each.second;
        L[each.first] = L[s] + 1;
        dfs(each.first);
    }
}

int lca(int s, int d) {
    while(S[s] != S[d]) {
        if(L[s] > L[d]) {
            s = S[s];
        }
        else {
            d = S[d];
        }
    }

    while(s != d) {
        if(L[s] > L[d]) {
            s = P[s];
        }
        else {
            d = P[d];
        }
    }
    return s;
}

int N; // number of nodes
// usage
memset(L, -1, sizeof L);
sqr = sqrt(N);
memset(L, 0, sizeof L);
memset(S, 0, sizeof S);
memset(dis, 0, sizeof dis);
dfs(0); // 0 is the root node in this case, change accordingly to the problem