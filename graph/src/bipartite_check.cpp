// int V, E;
// int color[N];
// vector<vector<int> > adj;
// memset(color, -1, sizeof color);

bool isBipartite(int u, int c) {                       // u = current vertex, c = u's color
    color[u] = c;
    for(int v : adj[u]) {
        if(color[v] == -1 and !isBipartite(v, 1 - c)) // Check if painting v with it's oposite color (1 - c) the graph is still bipartite;
            return false;                             // v hasn't been painted yet
        else if(color[u] == color[v])                 // u and v have equal colors. Graph is not bipartite.
            return false;
    }
    return true;                                      // didn't fail any check, graph is bipartite.
}
