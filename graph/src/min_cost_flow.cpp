#include <bits/stdc++.h>

using namespace std;

const int MAX = 5e2 + 5;
const int INF = 1e9;

struct rib {
    int b, u, c, f;
    int back;
};

int N, M, K;
int S, T;
vector<rib> adj[MAX];

inline void add_rib(int a, int b, int cap, int cost) {
    rib r1 = {b, cap, cost, 0, (int) adj[b].size()};
    rib r2 = {a, 0, -cost, 0, (int) adj[a].size()};
    adj[a].push_back(r1);
    adj[b].push_back(r2);
}

// With the residual network built, returns the flow of 'size' equals to desired_flow with minimum cost.
// Take care with N, as it is the number of nodes in the network but may not be the actually given in the problem.
int min_cost_flow(int source, int sink, int desired_flow) {
    int flow = 0, cost = 0;
    while(flow < desired_flow) {
        vector<int> id(N + 1, 0);
        vector<int> d(N + 1, INF);
        vector<int> q(N + 1);
        vector<int> p(N + 1);
        vector<int> p_rib(N + 1);
        int qh = 0, qt = 0;
        q[qt++] = source;
        d[source] = 0;
        while(qh != qt) {
            int v = q[qh++];
            id[v] = 2;
            if(qh == N) {
                qh = 0;
            }
            for(int i = 0; i < (int) adj[v].size(); ++i) {
                rib &r = adj[v][i];
                if(r.f < r.u && d[v] + r.c < d[r.b]) {
                    d[r.b] = d[v] + r.c;
                    if(id[r.b] == 0) {
                        q[qt++] = r.b;
                        if(qt == N) {
                            qt = 0;
                        }
                    }
                    else if(id[r.b] == 2) {
                        if(--qh == -1) {
                            qh = N - 1;
                        }
                        q[qh] = r.b;
                    }
                    id[r.b] = 1;
                    p[r.b] = v;
                    p_rib[r.b] = i;
                }
            }
        }
        if(d[sink] == INF) {
            break;
        }
        int addflow = desired_flow - flow;
        for(int v = sink; v != source; v = p[v]) {
            int pv = p[v];
            int pr = p_rib[v];
            addflow = min(addflow, adj[pv][pr].u - adj[pv][pr].f);
        }
        for(int v = sink; v != source; v = p[v]) {
            int pv = p[v];
            int pr = p_rib[v], r = adj[pv][pr].back;
            adj[pv][pr].f += addflow;
            adj[v][r].f -= addflow;
            cost += adj[pv][pr].c * addflow;
        }
        flow += addflow;
    }
    return cost;
}

int main() {
    return 0;
}

