const int MAX = 5e4 + 5;
constexpr int LOGMAX = 31 - __builtin_clz(MAX) + 5;
constexpr int BLOCK_SIZE = sqrt(2 * MAX) + 5;
int N, Q;
int color_app[MAX];
int app_app[MAX], father[MAX], P[LOGMAX][MAX], L[MAX], color[MAX], guys_app[MAX];
int st[MAX], en[MAX];
vector<ii> adj[MAX];
int dfs_c = 1;
int guys[2 * MAX];

void dfs(int source) {
    guys[dfs_c] = source;
    st[source] = dfs_c++;
    for(auto &each : adj[source]) {
        if(each.first != father[source]) {
            father[each.first] = source;
            L[each.first] = L[source] + 1;
            color[each.first] = each.second;
            dfs(each.first);
        }
    }
    guys[dfs_c] = source;
    en[source] = dfs_c++;
}

int lca(int u, int v) {
    if(L[u] > L[v]) {
        swap(u, v);
    }

    int lg = 31 - __builtin_clz(L[v]);
    ford(i, lg, 0) {
        if(L[v] - (1 << i) >= L[u]) {
            v = P[i][v];
        }
    }
    
    if(u == v) {
        return u;
    }

    lg = 31 - __builtin_clz(L[v]);
    ford(i, lg, 0) {
        if(P[i][u] != P[i][v]) {
            u = P[i][u];
            v = P[i][v];
        }
    }
    return father[u];
}

typedef struct query {
    int l, r, id, dis;
    query() {}
    query(int _l, int _r, int _id, int _dis) : l(_l), r(_r), id(_id), dis(_dis) {}
} query;

ld ans[MAX];
int max_app = 0;

inline void add(int k) {
    guys_app[k]++;
    if(guys_app[k] == 1) {
        app_app[color_app[color[k]]]--;
        color_app[color[k]]++;
        app_app[color_app[color[k]]]++;
        max_app = max(max_app, color_app[color[k]]);
    }
    else if(guys_app[k] == 2) {
        app_app[color_app[color[k]]]--;
        if(color_app[color[k]] == max_app && app_app[color_app[color[k]]] == 0) {
            max_app--;
        }
        color_app[color[k]]--;
        app_app[color_app[color[k]]]++;
    }
}

inline void remove(int k) {
    guys_app[k]--;
    if(guys_app[k] == 1) {
        app_app[color_app[color[k]]]--;
        color_app[color[k]]++;
        app_app[color_app[color[k]]]++;
        max_app = max(max_app, color_app[color[k]]);
    }
    else if(guys_app[k] == 0) {
        app_app[color_app[color[k]]]--;
        if(color_app[color[k]] == max_app && app_app[color_app[color[k]]] == 0) {
            max_app--;
        }
        color_app[color[k]]--;
        app_app[color_app[color[k]]]++;
    }
}

int main(){
    scanf("%d %d", &N, &Q);
    fori(i, 0, N - 1) {
        int a, b, c;
        scanf("%d %d %d", &a, &b, &c);
        adj[a].emplace_back(b, c);
        adj[b].emplace_back(a, c);
    }
    memset(P, -1, sizeof P);
    memset(father, -1, sizeof father);
    L[1] = 0;
    dfs(1);

    fori(i, 1, N + 1) {
        P[0][i] = father[i];
    }
    fori(i, 1, LOGMAX) {
        fori(j, 1, N + 1) {
            if(P[i - 1][j] != -1) {
                P[i][j] = P[i - 1][P[i - 1][j]];
            }
        }
    }
    vector<query> queries;
    fori(i, 0, Q) {
        int u, v;
        scanf("%d %d", &u, &v);
        if(u == v) {
            ans[i] = 0.0;
            continue;
        }
        if(L[v] > L[u]) {
            swap(u, v);
        }
        int lc = lca(u, v);
        if(lc == v) {
            queries.emplace_back(en[u], en[v] - 1, i, L[u] + L[v] - 2 * L[lc]);
        }
        else {
            if(en[v] < st[u]) {
                swap(u, v);
            }
            queries.emplace_back(en[u], st[v], i, L[u] + L[v] - 2 * L[lc]);
        }
    }

    sort(queries.begin(), queries.end(), [] (const query &lhs, const query &rhs) {
        int lll = lhs.l / BLOCK_SIZE, rrr = rhs.l / BLOCK_SIZE;
        if(lll != rrr) {
            return lll < rrr;
        }
        return lhs.r < rhs.r;
    });

    int LL = 1, R = 0;
    for(auto &q : queries) {
        while(q.l > LL) {
            remove(guys[LL++]);
        }
        while(LL > q.l) {
            add(guys[--LL]);
        }
        while(q.r > R) {
            add(guys[++R]);
        }
        while(R > q.r) {
            remove(guys[R--]);
        }
        ans[q.id] = (ld) max_app / q.dis;
    }

    fori(i, 0, Q) {
        cout << fixed << setprecision(10) << (ld) 100.0 * ans[i] << '\n';
    }

    return 0;
}