const int MAX = 1e5 + 5;
vector<ii> adj[MAX];
int n, m;
list<int> cyc;
void tour(int source, list<int>::iterator i) {
    for(auto &each : adj[source]) {
        if(each.second) {
            each.second = 0;
            for(auto &other : adj[each.first]) {
                if(other.first == source) {
                    other.second = 0;
                    break;
                }
            }
            tour(each.first, cyc.insert(i, source));
        }
    }
}
int main() {
    scanf("%d %d", &n, &m);
    fori(i, 0, m) {
        int u, v;
        scanf("%d %d", &u, &v);
        adj[u].emplace_back(v, 1);
        adj[v].emplace_back(u, 1);
    }
    int source = -1, ending = -1;
    int odds = 0;
    fori(i, 1, n + 1) {
        if(adj[i].size() & 1) {
            odds++;
            if(source == -1) {
                source = i;
            }
            else {
                ending = i;
            }
        }
    }
    if(source == -1) {
        source = 1;
        ending = 1;
    }
    if(odds == 1 || odds > 2) {
        cout << "DUMB!" << endl;
        return 0;
    }
    tour(source, cyc.begin());
    reverse(cyc.begin(), cyc.end());
    cyc.push_back(ending);
    for(auto it = cyc.begin(); it != cyc.end(); it++) {
        cout << *it << " ";
    }
    cout << endl;
    return 0;
}

