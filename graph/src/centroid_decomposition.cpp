int calc_size(int source, int father) {
    sz[source] = 1;
    tree_size++;
    for(auto &each : adj[source]) {
        if(each != father && !computed[each]) {
            sz[source] += calc_size(each, source);
        }
    }
    return sz[source];
}

int find_centroid(int source, int father) {
    for(auto &each : adj[source]) {
        if(each != father && !computed[each] && sz[each] > tree_size / 2) {
            return find_centroid(each, source);
        }
    }
    return source;
}

void build_centroid(int source, int last_centroid) {
    tree_size = 0;
    calc_size(source, -1);

    int centroid = find_centroid(source, source);
    computed[centroid] = true;
    centroid_father[centroid] = last_centroid;
    if(last_centroid != -1) {
        centroid_tree[last_centroid].push_back(centroid);
    }
    else {
        centroid_root = centroid;
    }

    for(auto &each : adj[centroid]) {
        if(!computed[each]) {
            build_centroid(each, centroid);
        }
    }
}