\contentsline {section}{Data Structures}{3}{section*.2}
\contentsline {subsection}{PBDS Tree}{3}{section*.3}
\contentsline {section}{Graph}{4}{section*.4}
\contentsline {subsection}{DFS applications}{4}{section*.5}
\contentsline {subsubsection}{Bipartite Check}{4}{section*.6}
\contentsline {subsubsection}{Articulation points and bridges}{5}{section*.7}
\contentsline {subsubsection}{Strongly connected components}{7}{section*.8}
\contentsline {subsection}{Trees}{9}{section*.9}
\contentsline {subsubsection}{DSU on Tree}{9}{section*.10}
\contentsline {subsubsection}{LCA}{11}{section*.11}
\contentsline {section}{Geometry}{14}{section*.12}
\contentsline {subsection}{Points and Lines}{14}{section*.13}
\contentsline {subsection}{Trigonometry}{15}{section*.14}
\contentsline {subsection}{Circle}{16}{section*.15}
\contentsline {subsection}{Polygon}{18}{section*.16}
\contentsline {section}{Mathematics}{19}{section*.17}
\contentsline {subsection}{Number Theory}{19}{section*.18}
\contentsline {subsubsection}{Greatest common divisor and Least common multiple}{19}{section*.19}
\contentsline {subsubsection}{Euler Totient Function from 1 to N}{20}{section*.20}
\contentsline {section}{Dynamic Progamming}{21}{section*.21}
\contentsline {subsection}{Max range sum (1D and 2D)}{21}{section*.22}
\contentsline {subsubsection}{MAX 1D - Kadane}{21}{section*.23}
\contentsline {subsubsection}{MAX 2D - Kadane}{22}{section*.24}
\contentsline {subsubsection}{Longest Increasing Subsequence (LIS)}{23}{section*.25}
\contentsline {subsubsection}{Knapsack 0-1}{24}{section*.26}
