fat[0] = fat[1] = inv[1] = inv_fat[0] = inv_fat[1] = 1;
fori(i, 2, MAX_N) {
    fat[i] = mult(fat[i - 1], i);
    int q = MOD / i, r = MOD % i;
    inv[i] = mult(-q, inv[r]);
    inv[i] += MOD;
    inv_fat[i] = mult(inv_fat[i - 1], inv[i]);
}