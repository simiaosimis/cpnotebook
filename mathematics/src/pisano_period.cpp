ll pisano(ll n) {
    ll prev = 1, cur = 1, ans = 1;
    while(prev != 0 || cur != 1) {
        ll temp = (prev + cur) % n;
        prev = cur;
        cur = temp;
        ans++;
    }
    return ans;
}