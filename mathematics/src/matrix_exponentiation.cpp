ll mat[MAX][MAX], id[MAX][MAX];
ll base[MAX][MAX], tmp[MAX][MAX]

inline void mult(ll A[MAX][MAX], ll B[MAX][MAX], ll C[MAX][MAX]) {
    fori(i, 0, N) {
        fori(j, 0, N) {
            ll cur = 0;
            fori(k, 0, N) {
                cur = (cur + ((A[i][k] * B[k][j]) % MOD));
            }
            C[i][j] = cur % MOD;
        }
    }
}

void mat_exp(ll exp) {
    memcpy(base, mat, sizeof base);
    memcpy(mat, id, sizeof mat);

    while(exp) {
        if(exp & 1) {
            memcpy(tmp, mat, sizeof tmp);
            mult(tmp, base, mat);
            exp--;
        }
        else {
            memcpy(tmp, base, sizeof tmp);
            mult(tmp, tmp, base);
            exp >>= 1;
        }
    }
}