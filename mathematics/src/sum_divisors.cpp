// NEEDS SIEVE !!!
ll sum_div(ll n) {
	if(n == 1) {
		return 1;
	}
    if(n <= MAX_PRIME && prime[n]) {
        return (n + 1);
    }

    ll sum = 1;
    for(int i = 0; i < pc && (primes[i] * primes[i]) <= n; i++) {
        ll multiplier = primes[i];
        while(!(n % primes[i])) {
            n /= primes[i];
            multiplier *= primes[i];
        }
        sum *= (multiplier - 1) / (primes[i] - 1);
    }

    if(n != 1) {
        sum *= (n * n - 1) / (n - 1);
    }

    return sum;
}
