ll mult(ll a, ll b, ll mod) {
    a %= mod;
    if(b > 0) {
        if(b & 1) {
            return (a + mult(a, b - 1, mod)) % mod;
        }
        else {
            return mult(a << 1, b >> 1, mod) % mod;
        }
    }
    return 0;
}

// miller rabin
bool is_prime(ll n) {
    if(n <= 1) {
        return false;
    }
    if(!(n & 1)) {
        return n == 2;
    }
    ll d = n - 1;
    ll s = 0;
    while(!(d & 1)) {
        d >>= 1;
        s++;
    }
    bool ret = true;
    for(ll i = 3; i <= min(20LL, n - 1); i++) {
        bool cur = false;
        if(f_exp(i, d, n) == 1) {
            cur = true;
        }
        else {
            for(ll j = 0; j <= s - 1; j++) {
                if(f_exp(i, (1LL << j) * d, n) == n - 1) {
                    cur = true;
                    break;
                }
            }
        }
        ret *= cur;
    }
    return ret;
}
