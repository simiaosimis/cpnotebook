const int N = 1e5;
int phi[N + 5];
int lp[N + 5];
vector<int> primes;

void sieve() {
    phi[1] = 1;
    for(int i = 2; i <= N; ++i) {
        if(lp[i] == 0) {
            lp[i] = i;
            phi[i] = i - 1;
            primes.push_back(i);
        }
        else {
            if (lp[i] == lp[i / lp[i]]) {
                phi[i] = phi[i / lp[i]] * lp[i];
            }
            else {
                phi[i] = phi[i / lp[i]] * (lp[i] - 1);
            }
        }
        for(ll j = 0; j < (int) primes.size() && primes[j] <= lp[i] && i * primes[j] <= N; ++j) {
            lp[i * primes[j]] = primes[j];
        }
    }
}


