int maxSum(vector<int> &a) { // vector of elements
                             // works for negative numbers
    int sum = a[0], maxS = sum;
    for(int i = 1; i < (int) a.size(); ++i) {
        sum = max(a[i], sum + a[i]);
        maxS = max(maxS, sum);
    }
    return maxS; // return max 1d sum
}
