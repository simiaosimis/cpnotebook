#include <bits/stdc++.h>

using namespace std;

const int MAX = 100000;

vector<int> a, parent;

int lis(int &endingPos) {
    int aSize = a.size();
    int maxLength = endingPos = 0;
    vector<int> lValue(aSize), lId(aSize);
    parent.resize(aSize);

    for(int i = 0; i < aSize; ++i) {
        int pos = lower_bound(lValue.begin(), lValue.begin() + maxLength, a[i]) - lValue.begin();
        // use upper_bound instead for longest non-decreasing subsequence
        lValue[pos] = a[i];
        lId[pos] = i;
        parent[i] = pos ? lId[pos - 1] : i;
        if(pos + 1 >= maxLength) {
            maxLength = pos + 1;
            endingPos = i;
        }
    }
    return maxLength;
}

void constructSolution(int endingPos) {
    int next = endingPos;
    stack<int> solution;
    for( ; next != parent[next]; next = parent[next]) solution.push(a[next]);
    solution.push(a[next]);
    for( ; !solution.empty(); solution.pop()) printf("%d\n", solution.top());
}

int main() {
    int v;
    while(scanf("%d", &v) == 1) a.push_back(v);
    int maxEnding = 0;
    int maxLength = lis(maxEnding);
    
    printf("%d\n-\n", maxLength);
    constructSolution(maxEnding);
    return 0;
}

