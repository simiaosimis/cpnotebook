ll dp[MAX_K][MAX_N];
ll partial[MAX_N];
int opt[MAX_K][MAX_N];

inline ll cost(int i, int j) {
    return (j - i + 1) * (partial[j] - partial[i - 1]);
}

inline void solve(int segments, int pos, int opt_left, int opt_right) {
    dp[segments][pos] = 1LL << 62;
    int limit = min(pos, opt_right + 1);
    fori(i, opt_left, limit) {
        if(dp[segments][pos] > dp[segments - 1][i] + cost(i + 1, pos)) {
            dp[segments][pos] = dp[segments - 1][i] + cost(i + 1, pos);
            opt[segments][pos] = i;
        }
    }
}

void roll(int segments, int left, int right, int opt_left, int opt_right) {
    if(left <= right) {
        int mid = (left + right) / 2;
        solve(segments, mid, opt_left, opt_right);
        roll(segments, left, mid - 1, opt_left, opt[segments][mid]);
        roll(segments, mid + 1, right, opt[segments][mid], opt_right);
    }
}

int main() {
    scanf("%d %d", &N, &K);
    K = min(N, K);
    fori(i, 1, N + 1) {
        scanf("%lld", &partial[i]);
        partial[i] += partial[i - 1];
    }

    fori(i, 1, N + 1) {
        dp[1][i] = partial[i] * i;
    }
    fori(i, 2, K + 1) {
        roll(i, i, N, i - 1, N); 
    }

    printf("%lld\n", dp[K][N]);

    return 0;
}