#include <bits/stdc++.h>

using namespace std;

const int MAX =  1010;

int dp[MAX][MAX],
    w[MAX],
    v[MAX],
    N;

int knapsack(int idx, int weigth) {
    if(idx == N or weigth == 0)
        return 0;
    if(dp[idx][weigth] != -1)
        return dp[idx][weigth];
    if(w[idx] > weigth)
        return dp[idx][weigth] = knapsack(idx + 1, weigth);
    return max(knapsack(idx + 1, weigth), v[idx] + knapsack(idx + 1, weigth - w[idx]));
}

int main() {
    ios_base::sync_with_stdio(false);

    int t;
    cin >> t;

    while(t--) {
        cin >> N;
        for(int i = 0; i < N; ++i) {
            cin >> v[i] >> w[i];
        }

        int capacity;
        cin >> capacity;

        memset(dp, -1, sizeof dp);
        int ans = knapsack(0, capacity);
        cout << ans << "\n";
    }
    return 0;
}

