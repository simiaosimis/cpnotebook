#include <bits/stdc++.h>

using namespace std;

const int MAX =  105;
const int INF = 2147483640;

int A[MAX][MAX];
int N;

inline int kadane1D(vector<int> &a) {
    int sum = a[0], maxSum = a[0];
    for(int i = 1; i < (int) a.size(); ++i) {
        sum = max(sum + a[i], a[i]);
        maxSum = max(maxSum, sum);
    }
    return maxSum;
}

inline int max2DSum() {
    int maxSum = -INF;
    for(int i = 0; i < N; ++i) {
        vector<int> a(N);
        for(int j = i; j < N; ++j) {
            for(int k = 0; k < N; ++k) a[k] += A[k][j];
            int sum = kadane1D(a);
            maxSum = max(maxSum, sum);
        }
    }
    return maxSum;
}

int main() {
    while(scanf("%d", &N) == 1) {
        for(int i = 0; i < N; ++i) for(int j = 0; j < N; ++j)
            scanf("%d", &A[i][j]);
        printf("%d\n", max2DSum());
    }
    return 0;
}

