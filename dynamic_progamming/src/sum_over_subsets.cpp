const int N = 5;
int A[N], F[N];

int main() {

    // O(3^N)
    for (int mask = 0; mask < (1 << n); mask++){
        F[mask] = A[0];
        for(int i = mask; i > 0; i = (i - 1) & mask) {
            F[mask] += A[i];
        }
    }

    // O(2^N * N)
    // After the loops, F[i] will hold the sum of all A[j] such that i & j = j, i.e. the mask j is a submask of the mask i.
    fori(i, 0, 1 << N) {
        F[i] = A[i];
    }
    fori(i, 0, N) {
        fori(mask, 0, 1 << N) {
            if(mask & (1 << i)) {
                F[mask] += F[mask ^ (1 << i)];
            }
        }
    }


    return 0;
}