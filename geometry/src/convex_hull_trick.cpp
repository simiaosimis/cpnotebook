vector<ll> slope;
vector<ll> yy;

// Use this when there is overflow risk. Less safe because of float-point comparation.
inline bool bad(int l1, int l2, int l3) {
    return ((ld) (yy[l2] - yy[l1])) / (slope[l1] - slope[l2]) > ((ld) (yy[l3] - yy[l1])) / (slope[l1] - slope[l3]);
}

// Use this when there is no overflow risk.
inline bool bad(int l1, int l2, int l3) {
    return (yy[l2] - yy[l1]) * (slope[l1] - slope[l3]) > (yy[l3] - yy[l1]) * (slope[l1] - slope[l2]);
}

inline void add(ll a, ll b) {
    slope.push_back(a);
    yy.push_back(b);
    while(slope.size() >= 3 && bad(slope.size() - 3, slope.size() - 2, slope.size() - 1)) {
        slope.erase(slope.end() - 2);
        yy.erase(yy.end() - 2);
    }
}

// Overrall O(Q) query. In this case, slope is decreasing and query will return the lowest ax + b value for some line.
int ptr;
inline ll query(ll x) {
    if(ptr >= (int) slope.size()) {
        ptr = slope.size() - 1;
    }
    while(ptr < (int) slope.size() - 1 && slope[ptr] * x + yy[ptr] >= slope[ptr + 1] * x + yy[ptr + 1]) {
        ptr++;
    }
    return slope[ptr] * x + yy[ptr];
}

// binary search query, when x is not increasing or decreasing.
inline ll query(ll x) {
    int l = 0, r = slope.size() - 1;
    while(l < r) {
        int mid = (l + r) / 2;
        if(slope[mid] * x + yy[mid] < slope[mid + 1] * x + yy[mid + 1]) {
            l = mid + 1;
        }
        else {
            r = mid;
        }
    }
    return slope[l] * x + yy[l];
}


// Example 1
fori(i, 1, n + 1) {
        sum[i] = sum[i - 1] + t[i];
        inv[i] = inv[i - 1] + (ld) 1.0 / t[i];
        pre[i] = pre[i - 1] + ((ld) sum[i] / t[i]);
        dp[1][i] = pre[i];
    }

    // dp[i][j] = minimum value using i groups until element j
    fori(i, 2, k + 1) {
        slope.clear();
        yy.clear();
        ptr = 0;
        dp[i][1] = dp[i - 1][1];
        add(-sum[1], dp[i - 1][1] - pre[1] + sum[1] * inv[1]);
        fori(j, 2, n + 1) {
            dp[i][j] = pre[j] + query(inv[j]);
            add(-sum[j], dp[i - 1][j] - pre[j] + sum[j] * inv[j]);
        }
    }

    cout << fixed << setprecision(12) << dp[k][n] << '\n';


// Example 2
fori(i, 1, m + 1) {
    dp[1][i] = f[i] * i - s[i];
}

fori(j, 2, p + 1) {
    slope.clear();
    yy.clear();
    add(-j + 1, dp[j - 1][j - 1] + s[j - 1]);
    fori(i, j, m + 1) {
        dp[j][i] = f[i] * i - s[i] + query(f[i]);
        add(-i, dp[j - 1][i] + s[i]);
    }
}
