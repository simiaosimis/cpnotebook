double line_sweep(){
	if(points.size() == 1) return 100000;
	double h = dist(points[0], points[1]);
	set<ii> candidates;
	candidates.insert(ii(points[0].y, points[0].x));
	candidates.insert(ii(points[1].y, points[1].x));
	p0 = points[0];
	p1 = points[1];
	fori(i,2,points.size()){
		for(auto j : candidates){
			if(points[i].x - j.y > h){
				candidates.erase(j);
			}
			else break;
		}
		auto it = candidates.lower_bound(ii(points[i].y - h, 0));
		for(;it != candidates.end(); it++){
			ii cur = *it;
			double d = dist(points[i], ii(cur.y, cur.x));
			if(d < h){
				h = d;
				p0 = points[i];
				p1 = ii(cur.y, cur.x);
			}
			if(cur.x > points[i].y + h) break;
		}
		candidates.insert(ii(points[i].y, points[i].x));
	}
	return h;
}
