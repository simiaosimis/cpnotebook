typedef pair<int, int> ii;
#define x first
#define y second

ll cross(const ii &O, const ii &A, const ii &B){
    return (A.x - O.x)*(ll)(B.y - O.y) - (A.y - O.y)*(ll)(B.x - O.x);
}

template <class T> inline int sgn(const T& x) { return (T(0) < x) - (x < T(0)); }

int is_inside(const ii& point, const vector<ii>& poly, int top) {
  if (point < poly[0] || point > poly[top]) return 1;
  auto orientation = cross(poly[0], poly[top], point);
  if (orientation == 0) {
    if (point == poly[0] || point == poly[top]) return 0;
    return top == 1 || top + 1 == poly.size() ? 0 : -1;
  } else if (orientation < 0) {
    auto itRight = lower_bound(begin(poly) + 1, begin(poly) + top, point);
    return sgn(cross(itRight[-1], point, itRight[0]));
  } else {
    auto itLeft = upper_bound(poly.rbegin(), poly.rend() - top-1, point);
    return sgn(cross(itLeft[0], point, itLeft == poly.rbegin() ? poly[0] : itLeft[-1]));
  }
}
