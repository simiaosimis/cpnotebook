ll cross(const ii &O, const ii &A, const ii &B)
{
    return (A.x - O.x) * (ll)(B.y - O.y) - (A.y - O.y) * (ll)(B.x - O.x);
}
 
vector<ii> convex_hull(vector<ii> P)
{
    int n = P.size(), k = 0;
    vector<ii> H(2*n);
 
    sort(P.begin(), P.end());
 
    // Build lower hull
    for (int i = 0; i < n; i++) {
        while (k >= 2 && cross(H[k-2], H[k-1], P[i]) < 0) k--;
        H[k++] = P[i];
    }
 
    H.resize(k);
    return H;
}

int main() {
    ios_base::sync_with_stdio(false);

	int n; cin >> n;
	vector<ii> v;
	map<ii, int> m;
	bool is = true;
	if(n==2) is = false;
	fori(i,0,n){
		int x, y; cin >> x >> y;
		v.push_back(ii(x,y));
		m[ii(x,y)] = i+1;
	}
	int cur = 0;
	fori(i,2,n){
		if(!cross(v[i],v[i-1],v[i-2])) cur++;
	}
	if(cur == n-2) is = false;
	vector<ii> lower = convex_hull(v);
	fori(i,0,lower.size()){
		v.erase(find(v.begin(), v.end(), lower[i]));
	}
	sort(v.begin(), v.end());
	vector<int> ans;
	ans.push_back(m[lower[0]]);
	fori(i,0,v.size()){
		ans.push_back(m[v[i]]);
	}
	ford(i,lower.size()-1,0){
		ans.push_back(m[lower[i]]);
	}
	if(!is){
		cout << "-1" << endl;
	}
	else{
		cout << ans.size()-1 << endl;
		fori(i,1,ans.size()){
			cout << ans[i] << " " << ans[i-1] << endl;
		}
	}

    return 0;
}
