typedef pair<long double, long double> pdd;
typedef pair<pdd, long double> pddd;
typedef pair<long double, pdd> pii;
#define x first
#define y second

pdd operator + (const pdd p1, const pdd p2){
	return pdd(p1.x + p2.x, p1.y + p2.y);
}
pdd operator - (const pdd p1, const pdd p2){
	return pdd(p1.x - p2.x, p1.y - p2.y);
}

pdd operator * (const long double c, const pdd p){
	return pdd(p.x * c, p.y * c);
}
pdd operator - (const pdd p){
	return (-1.0) * p;
}
long double operator * (const pdd p1, const pdd p2){
	return p1.x * p2.x + p1.y * p2.y;
}
long double operator % (const pdd p1, const pdd p2){
	return p1.x * p2.y - p2.x * p1.y;
}

struct Mcc{
  // return pair of center and r^2
  int n;
  vector<pdd> p;
  pdd cen;
  long double r2;
  
  void init(int _n, vector<pdd> _p){
    n = _n;
    p = _p;
  }
  long double sqr(long double a){ return a*a; }
  long double abs2(pdd a){ return a*a; }
  pdd center(pdd p0, pdd p1, pdd p2) {
    pdd a = p1-p0;
    pdd b = p2-p0;
    long double c1=abs2(a)*0.5;
    long double c2=abs2(b)*0.5;
    long double d = a % b;
    long double x = p0.x + (c1 * b.y - c2 * a.y) / d;
    long double y = p0.y + (a.x * c2 - b.x * c1) / d;
    return pdd(x,y);
  }

  pair<pdd,long double> solve(){
    random_shuffle(p.begin(),p.end());
    r2=0;
    for (int i=0; i<n; i++){
      if (abs2(cen-p[i]) <= r2) continue;
      cen = p[i];
      r2 = 0;
      for (int j=0; j<i; j++){
        if (abs2(cen-p[j]) <= r2) continue;
        cen = 0.5 * (p[i]+p[j]);
        r2 = abs2(cen-p[j]);
        for (int k=0; k<j; k++){
          if (abs2(cen-p[k]) <= r2) continue;
          cen = center(p[i],p[j],p[k]);
          r2 = abs2(cen-p[k]);
        }
      }
    }
    return {cen,r2};
  }
}mcc;