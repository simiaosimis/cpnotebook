void circumcircle(const ii p0, const ii p1, const ii p2, ii &center){
    double dA, dB, dC, aux1, aux2, div;
 
    dA = p0.x * p0.x + p0.y * p0.y;
    dB = p1.x * p1.x + p1.y * p1.y;
    dC = p2.x * p2.x + p2.y * p2.y;
 
    aux1 = (dA*(p2.y - p1.y) + dB*(p0.y - p2.y) + dC*(p1.y - p0.y));
    aux2 = -(dA*(p2.x - p1.x) + dB*(p0.x - p2.x) + dC*(p1.x - p0.x));
    div = (2*(p0.x*(p2.y - p1.y) + p1.x*(p0.y-p2.y) + p2.x*(p1.y - p0.y)));
 
    if(fabs(div) <= EPS){ return ; }
    center.x = aux1/div;
    center.y = aux2/div;
}
