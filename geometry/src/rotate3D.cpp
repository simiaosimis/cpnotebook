typedef pair<double, double> dd;
typedef pair<double, dd> ddd;
#define x first
#define y second.first
#define z second.second

//rotate vector p around unitary vector u by angle t
void rotateAroundAxis(double t, ddd &p, ddd u){
	ddd aux;
	aux.x = (u.x*u.x + (1-u.x*u.x)*cos(t)) * p.x 
		+   (u.x*u.y*(1-cos(t)) - u.z*sin(t)) * p.y 
		+   (u.x*u.z*(1 - cos(t)) + u.y*sin(t)) * p.z;
	aux.y = (u.x*u.y*(1-cos(t)) + u.z*sin(t)) * p.x 
		+   (u.y*u.y + (1-u.y*u.y)*cos(t)) * p.y 
		+   (u.y*u.z*(1 - cos(t)) - u.x*sin(t)) * p.z;
	aux.z = (u.x*u.z*(1-cos(t)) - u.y*sin(t)) * p.x 
		+   (u.y*u.z*(1-cos(t)) + u.x*sin(t)) * p.y 
		+   (u.z*u.z + (1 - u.z*u.z) * cos(t)) * p.z;
	p = aux;
}
