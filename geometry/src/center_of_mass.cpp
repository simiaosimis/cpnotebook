int main(){
	ios_base::sync_with_stdio(false);

	int n;
	while(cin >> n, n > 2){
		vector<ii> v;
		fori(i,0,n){
			double a, b; cin >> a >> b;
			v.emplace_back(a,b);
		}
		v = convex_hull(v);
		double xsum = 0.0;
		double ysum = 0.0;
		double area = 0.0;
		fori(i,0,v.size()-1) {
    		ii p0 = v[i];
		    ii p1 = v[i+1];

		    double areaSum = (p0.x * p1.y) - (p1.x * p0.y);

		    xsum += (p0.x + p1.x) * areaSum;
		    ysum += (p0.y + p1.y) * areaSum;
		    area += areaSum;
		}

		double cx = xsum / (area * 6);
		double cy = ysum / (area * 6);
		cout << fixed << setprecision(3) << 2*cx << " " << 2*cy << endl;
	}

	return 0;
}

