#define x first
#define y second
#define EPS 1e-9
ld PI = acos(-1.0);
ld intersectionAreaCircles(const ii center0, const ld r1, const ii center1, const ld r2) {
	const ld distance = hypot(center0.x - center1.x, center0.y - center1.y);	// Euclidean distance between the two center points
	if (distance > r1 + r2) {
		// Circles do not intersect
		return 0.0;
	}
	if (distance <= fabs(r1 - r2)) {
		// One circle is contained completely inside the other, just return the smaller circle area 
		const ld A0 = PI * r1 * r1;
		const ld A1 = PI * r2 * r2;

		return r1 < r2 ? A0 : A1;
	}
	if (fabs(distance) < EPS && fabs(r1 - r2) < EPS) {
		// Both circles are equal, just return the circle area
		return PI * r1 * r1;
	}
	ld a = acos((r1 * r1 - r2 * r2 + distance * distance) / (2 * distance * r1)); // First center point to the middle line
	ld b = acos((r2 * r2 - r1 * r1 + distance * distance) / (2 * distance * r2)); // Second center point to the middle line
    ld s1 = a * r1 * r1;	
    ld s2 = b * r2 * r2;
    ld t1 = r1 * r1 * sin(a) * cos(a);
    ld t2 = r2 * r2 * sin(b) * cos(b);
	return s1 + s2 - t1 - t2;
}
