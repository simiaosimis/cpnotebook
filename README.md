# CP notebook
### Installing latex dependencies

**Ubuntu 16.04**

In case you don't have textlive installed, you can install the bare minimum dependencies with:

```sh
$ sudo apt-get install texlive-latex-base
$ sudo apt-get install texlive-latex-extra --no-install-recommends
```
### Generating the PDF output

Inside the main folder *(/cpnotebook)* type:

```sh
$ pdflatex main.tex
```
That will create the output in file `main.pdf`, which you can open with:
```sh
$ xdg-open main.pdf
```

### Contributing

Suppose you want to contribute to an existing chapter (**Mathematics**, **Graph**, **String** and so on). Let's say that you'll add the BFS (comments + code) for the chapter Graph. 

First, you should add the source code (.cpp) to the respective /src folder, for example:
```sh
$ cp BFS.cpp cpnotebook/graph/src
```
Second, open the file responsable (*graph.tex*) for the Graph chapter  
```sh
$ cd /cpnotebook/graph
$ vim graph.tex
```
And then you create a subsection (or a subsubsection) for that chapater by appending the following lines:

```sh
\subsection{BFS}
Type here a comment for BFS subsection

\cppscript{graph/src/BFS}{O(N)}
\clearpage

```
Note that you refer to the BFS.cpp file but you must not write the file extension (.cpp). You can (optinal) also write the algorithm's complexity typing {O(N)} at that the end of the \cppscript line.

In case you want to create a new chapeter, take a look at the file /cpnotebook/main.tex for details.
