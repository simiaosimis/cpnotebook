#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace __gnu_pbds;

typedef tree<int, null_type, less<int>, rb_tree_tag, tree_order_statistics_node_update> oset;

oset s;

// usage:
s.insert(1);
s.insert(5);

// returns iterator to the k-th element
int k = 1234567;
auto it = s.find_by_order(k);

// returns the position of the element X (0-indexed).
// if the element doesn't exist in the set, returns the position of the "next" element.
int x = 1234;
int pos = s.order_of_key(x);