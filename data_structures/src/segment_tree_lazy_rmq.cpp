struct node {
    int val;
    node() : val(0) {} // neutral value
};

node st[4 * MAX];
int lazy[4 * MAX];

node merge(const node &a, const node &b) {
    node c;
    // do merge
    return c;
}

void build(int p, int l, int r) {
    if (l == r) {
        // set st[p] correctly
        return;
    }

    int m = (l + r) / 2;

    build(2 * p, l, m);
    build(2 * p + 1, m + 1, r);

    st[p] = merge(st[2 * p], st[2 * p + 1]);
}

void push(int p, int l, int r, int val, bool reset = true) {
    if (val != 0) { // should be neutral value
        st[p].val += val;

        if (l != r) {
            lazy[2 * p] += val;
            lazy[2 * p + 1] += val;
        }

        if (reset) lazy[p] = 0; // should be neutral value
    }
}

void update(int p, int l, int r, const int &i, const int &j, const int &new_val) {
    push(p, l, r, lazy[p]);

    if (j < l or r < i) {
        return;
    }

    if (i <= l and r <= j) {
        push(p, l, r, new_val, false);
        return;
    }

    int m = (l + r) / 2;

    update(2 * p, l, m, i, j, new_val);
    update(2 * p + 1, m + 1, r, i, j, new_val);

    st[p] = merge(st[2 * p], st[2 * p + 1]);
}

node query(int p, int l, int r, const int &i, const int &j) {
    push(p, l, r, lazy[p]);

    if (i <= l and r <= j) {
        return st[p];
    }

    if (r < i or l > j) {
        return node();
    }

    int m = (l + r) / 2;

    node a = query(2 * p, l, m, i, j);
    node b = query(2 * p + 1, m + 1, r, i, j);

    return merge(a, b);
}