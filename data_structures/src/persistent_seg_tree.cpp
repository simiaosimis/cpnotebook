const int MAX = 1e7 + 5;
int N, Q, arr[MAX], qnum[MAX], st[4 * MAX], L[4 * MAX], R[4 * MAX], root[MAX];
int next_idx = 1;

void build(int p, int l, int r) {
    if(l == r) {
        return;
    }
    L[p] = next_idx++;
    R[p] = next_idx++;
    int mid = (l + r) / 2;
    build(L[p], l, mid);
    build(R[p], mid + 1, r);
}

int update(int p, int l, int r, int idx, int val) {
    int new_p = next_idx++;
    if(l == r && l == idx) {
        st[new_p] = st[p] + val;
        return new_p;
    }
    L[new_p] = L[p];
    R[new_p] = R[p];
    int mid = (l + r) / 2;
    if(idx <= mid) {
        L[new_p] = update(L[p], l, mid, idx, val);
    }
    else {
        R[new_p] = update(R[p], mid + 1, r, idx, val);
    }
    st[new_p] = st[L[new_p]] + st[R[new_p]];
    return new_p;
}

int query(int p, int l, int r, int i, int j) {
    if(l >= i && j >= r) {
        return st[p];
    }
    if(i > r || l > j) {
        return 0;
    }
    int mid = (l + r) / 2;
    return query(L[p], l, mid, i, j) + query(R[p], mid + 1, r, i, j);
}

int main() {
    root[0] = next_idx++;
    build(root[0], 1, N);
    fori(i, 0, N) {
        int val, idx;
        tie(val, idx) = v[i];
        root[i + 1] = update(root[i], 1, N, idx, 1);
        qnum[i + 1] = val;
    }

    return 0;
}

