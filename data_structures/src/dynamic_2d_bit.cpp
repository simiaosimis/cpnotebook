#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace __gnu_pbds;

typedef tree<ii, null_type, less<ii>, rb_tree_tag, tree_order_statistics_node_update> oset;

const int MAX = 1e5 + 5;
oset bit[MAX];

void update(int x, int y, int val) {
    int _x = x;
    while(x < MAX) {
        if(val == 1) {
            bit[x].insert(ii(y, _x));
        }
        else {
            bit[x].erase(ii(y, _x));
        }
        x += x & -x;
    }
}

int query(int x, int y) {
    int sum = 0;
    while(x) {
        sum += bit[x].order_of_key(ii(y + 1, 0));   
        x -= x & -x;
    }
    return sum;
}

