struct query {
    int l, r, id;
    query() {}
    query(int _l, int _r, int _id) : l(_l), r(_r), id(_id) {}
};

const int MAX = 1e5 + 5;
constexpr int BLOCK_SIZE = sqrt(MAX) + 10;

inline void add(int element);
inline void rem(int element);

sort(queries.begin(), queries.end(), [] (const query &lhs, const query &rhs) {
    int lll = lhs.l / BLOCK_SIZE, rrr = rhs.l / BLOCK_SIZE;
    if(lll != rrr) {
        return lll < rrr;
    }
    return lhs.r < rhs.r;
});

// The order of the while's is important! always put the R/q.r ones before the
// L/q.l ones as it may avoid runtime errors!!
int L = 1, R = 0;
for(auto &q : queries) {
    while(q.r < R) {
        rem(arr[R--]);  
    }
    while(R < q.r) {
        add(arr[++R]);  
    }
    while(q.l < L) {
        add(arr[--L]);      
    }
    while(L < q.l) {
        rem(arr[L++]);
    }
    ans[q.id] = max_app;
}

