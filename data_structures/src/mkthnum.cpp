// answer queries of the type: what is the K-th element in the (after ordered) range [L, R]
const int MAX = 4e6 + 6;
int arr[MAX], idx[MAX], ordered_pos[MAX], root[MAX], st[MAX], L[MAX], R[MAX];
int n, m, NEXT = 1; 

void build(int p, int l, int r) {
	if(l == r) {
		return;
	}
	L[p] = NEXT++;
	R[p] = NEXT++;
	build(L[p], l, (l + r) / 2);
	build(R[p], (l + r) / 2 + 1, r);
}

int update(int p, int l, int r, int pos) {
	int new_p = NEXT++;
	if(l == r) {
		st[new_p]++;
		return new_p;
	}
	L[new_p] = L[p];
	R[new_p] = R[p];
	int mid = (l + r) / 2;
	if(pos <= mid) {
		L[new_p] = update(L[p], l, mid, pos);
	}
	else {
		R[new_p] = update(R[p], mid + 1, r, pos);
	}
	st[new_p] = st[L[new_p]] + st[R[new_p]];
	return new_p;
}

int main() {
	scanf("%d %d", &n, &m);
	fori(i, 1, n + 1) {
		scanf("%d", arr + i);
		idx[i] = i;
	}

	sort(idx + 1, idx + n + 1, [] (int lhs, int rhs) {
		return arr[lhs] < arr[rhs];
	});

	fori(i, 1, n + 1) {
		ordered_pos[idx[i]] = i;
	}

	root[0] = NEXT++;
	build(root[0], 1, n);
	fori(i, 1, n + 1) {
		root[i] = update(root[i - 1], 1, n, ordered_pos[i]);
	}

	fori(t, 0, m) {
		int l, r, k;
		scanf("%d %d %d", &l, &r, &k);
		int left = 1, right = n;
		int left_root = root[l - 1], right_root = root[r];
		int ans = 0;
		while(k > 0) {
			if(left == right) {
				ans = arr[idx[left]];
				k--;
			}
			else if(st[L[right_root]] - st[L[left_root]] >= k) {
				left_root = L[left_root];
				right_root = L[right_root];
				right = (left + right) / 2;
			}
			else {
				k -= st[L[right_root]] - st[L[left_root]];
				left_root = R[left_root];
				right_root = R[right_root];
				left = (left + right) / 2 + 1;
			}
		}
		printf("%d\n", ans);
	}

	return 0;
}