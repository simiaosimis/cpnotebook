#include <bits/stdc++.h>

#define debug(x) cout << #x << " = " << x << endl
#define fori(i, ini, lim) for(int i = int(ini); i < int(lim); i++)
#define ford(i, ini, lim) for(int i = int(ini); i >= int(lim); i--)

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;

const int INF = (int) 2e9;
namespace juramdir_perera_treap {
	// A < B - left
	// A >= B - right
	struct node {
		int val, pri, cnt, sub, lazy;
		node *l, *r;
		node() : val(0), pri(0), cnt(0), sub(INF), lazy(0), l(nullptr), r(nullptr) {}
		node(int _val, int _pri) : val(_val), pri(_pri), cnt(0), sub(val), lazy(0), l(nullptr), r(nullptr) {}
	};
	typedef node * pnode;

	int rand_pri() {
		return rand();
	}

	int cnt(pnode root) {
		return root != nullptr ? root->cnt : 0;
	}

	int sub(pnode root) {
		return root != nullptr ? root->sub : INF;
	}

	void push(pnode root) {
		if(root != nullptr && root->lazy != 0) {
			root->val += root->lazy;
			root->sub += root->lazy;
			if(root->l != nullptr) {
				root->l->lazy += root->lazy;
			}
			if(root->r != nullptr) {
				root->r->lazy += root->lazy;
			}
			root->lazy = 0;
		}
	}

	void update(pnode root) {
		if(root != nullptr) {
			push(root);
			push(root->l);
			push(root->r);
			root->cnt = cnt(root->l) + cnt(root->r) + 1;
			root->sub = min({sub(root->l), sub(root->r), root->val});
		}
	}

	void split(pnode root, int key, pnode &l, pnode &r, int less) { // L contains elements less than key
		if(root == nullptr) {									// R contains elements greater than or equal key
			l = r = nullptr;										
			return;
		}
		push(root);
		int cur_key = less + cnt(root->l);
		if(key <= cur_key) {
			split(root->l, key, l, root->l, less);
			r = root;
		}
		else { 
			split(root->r, key, root->r, r, less + cnt(root->l) + 1);
			l = root;
		}
		update(root);
	}

	void merge(pnode &root, pnode l, pnode r) { // there is an assumption s.t. every element in
		push(l);								// treap L is less than every element in treap R.
		push(r);
		if(l == nullptr || r == nullptr) {  
			root = l != nullptr ? l : r;
		}
		else if(l->pri > r->pri) {
			merge(l->r, l->r, r);
			root = l;
		}
		else {
			merge(r->l, l, r->l);
			root = r;
		}
		update(root);
	}

	void insert(pnode &root, int index, int val) {
		pnode l = nullptr, r = nullptr;
		split(root, index, l, r, 0);
		pnode new_node = new node(val, rand_pri());
		merge(root, l, new_node);
		merge(root, root, r);
	}

	void erase(pnode &root, int key, int less) {
		push(root);
		int cur_key = less + cnt(root->l);
		if(key == cur_key) {
			merge(root, root->l, root->r); 
		}
		else if(cur_key > key) {
			erase(root->l, key, less);  
		}
		else {
			erase(root->r, key, less + cnt(root->l) + 1);
		}
		update(root);
	}

	int find_max(pnode root, int limit, int less) {
		push(root);
		push(root->l);
		push(root->r);
		int cur_key = less + cnt(root->l);
		if(sub(root->r) < limit) {
			return find_max(root->r, limit, less + cnt(root->l) + 1);
		}
		else if(root->val < limit) {
			return cur_key;
		}
		else {
			return find_max(root->l, limit, less);  
		}
	}

	void update(pnode root, int lindex, int rindex) { // add 1 to every element in segment [L, R]
		if(lindex > rindex) {
			return;
		}
		push(root);
		pnode t1 = nullptr, t2 = nullptr, t3 = nullptr;
		split(root, rindex + 1, t2, t3, 0); // split the treap into T1 = [0, L - 1],
		split(t2, lindex, t1, t2, 0);	   // T2 = [L, R] and T3 = [R + 1, N - 1]
		t2->lazy++;
		merge(t2, t1, t2);				  // then, merge them again into one single treap
		merge(root, t2, t3);				// the original one.
	}
};

using namespace juramdir_perera_treap;

int main() {
	srand(time(NULL));
	pnode root = nullptr;
	insert(root, 0, 0);
	int n;
	scanf("%d", &n);
	int size = 0;
	fori(i, 0, n) {
		int l, r;
		scanf("%d %d", &l, &r);
		int max_j = find_max(root, l, 0);
		int max_k = find_max(root, r, 0);
		update(root, max_j + 1, max_k);
		if(max_k < size) {
			erase(root, max_k + 1, 0);  
		}
		else {
			size++;
		}
		insert(root, max_j + 1, l);
	}
	printf("%d\n", size);

	return 0;
}

