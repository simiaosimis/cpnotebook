const int MAX = 2e5 + 5;
node st[4 * MAX];
int val[MAX];
int n;

struct node {
    node() {}
};

node merge(const node &a, const node &b) {
    node c;
    // do merge
    return c;
}

void build(int p, int l, int r) {
    if (l == r) {
        // set st[p] to initial value
        return;
    }

    int m = (l + r) / 2;

    build(2 * p, l, m);
    build(2 * p + 1, m + 1, r);

    st[p] = merge(st[2 * p], st[2 * p + 1]);
}

void update(int p, int l, int r, const int &idx, const int &new_val) {
    if (l == r and l == idx) {
        // set st[p] to new_val somehow
        return;
    }

    int m = (l + r) / 2;

    if (idx <= m) {
        update(2 * p, l, m, idx, new_val);
    } else {
        update(2 * p + 1, m + 1, r, idx, new_val);
    }

    st[p] = merge(st[2 * p], st[2 * p + 1]);
}

node query(int p, int l, int r, const int &i, const int &j) {
    if (i <= l and r <= j) {
        return st[p];
    }

    if (r < i or l > j) {
        // node() here should be the "neutral" value (-INF for max segtree, for example)
        return node();
    }

    int m = (l + r) / 2;

    node a = query(2 * p, l, m, i, j);
    node b = query(2 * p + 1, m + 1, r, i, j);

    return merge(a, b);
}
