inline ll in_range(int left, int right) {
    return pref[right] - pref[left - 1];
}

inline int min_query(int left, int right) {
    int sz = right - left + 1;
    int lg = 31 - __builtin_clz(sz);
    return min(st[lg][left], st[lg][right - (1 << lg) + 1]);
}

string roll(int l, int r, int len, ll k) {
    string ans = "";
    while(ans.empty()) {
        int left = l + 1, right = r;
        int edge = l;
        while(left <= right) {
            int mid = (left + right) / 2;
            if(min_query(l + 1, mid) >= len) {
                edge = mid;
                left = mid + 1;
            }
            else {
                right = mid - 1;
            }
        }
        ll total = in_range(l, edge) - 1LL * (len - 1) * (edge - l + 1);
        if(total < k) {
            l = edge + 1;
            k = k - total;
        }
        else if(k > (edge - l + 1)) {
            len++;
            k = k - (edge - l + 1);
        }
        else {
            int pos = l + k - 1;        
            fori(i, 0, len) {
                ans += T[sa[pos] + i];
            }
        }
    }
    return ans;
}

int main() {
    scanf("%s", T);
    N = strlen(T);

    ll k;
    scanf("%lld", &k);

    if((1LL * N * (N + 1)) / 2 < k) {
        puts("No such line.");
        return 0;
    }

    T[N++] = '$';
    build_sa();
    build_lcp();
    fori(i, 1, N) {
        pref[i] = pref[i - 1] + N - sa[i] - 1;
        st[0][i] = lcp[i];
    }
    fori(i, 1, LOGMAX) {
        for(int j = 1; j + (1 << i) - 1 < N; j++) {
            st[i][j] = min(st[i - 1][j], st[i - 1][j + (1 << (i - 1))]);
        }
    }

    printf("%s\n", roll(1, N - 1, 1, k).c_str());

    return 0;
}