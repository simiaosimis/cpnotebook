#include <bits/stdc++.h>

#define fori(i, ini, lim) for(int i = int(ini); i < int(lim); i++)

using namespace std;

const int MAX = 1e5 + 5;
int N;
char T[MAX];
int sa[MAX], tsa[MAX], ra[MAX], tra[MAX];
int lcp[MAX], plcp[MAX], phi[MAX], c[MAX];

void sort(int k) {
    memset(c, 0, sizeof c);
    fori(i, 0, N) {
        c[i + k < N ? ra[i + k] : 0]++; 
    }
    int sum = 0, maxi = max(300, N);
    fori(i, 0, maxi) {
        int t = c[i];
        c[i] = sum;
        sum += t;
    }
    fori(i, 0, N) {
        int cur = sa[i] + k < N ? ra[sa[i] + k] : 0; 
        tsa[c[cur]++] = sa[i];
    }
    memcpy(sa, tsa, sizeof(int) * N);
}

void build_sa() {
    fori(i, 0, N) {
        ra[i] = T[i];
        sa[i] = i;
    }
    for(int k = 1; k < N; k *= 2) {
        sort(k);
        sort(0);
        int r = tra[sa[0]] = 0;
        fori(i, 1, N) {
            if(ra[sa[i]] == ra[sa[i - 1]]) {
                int cur = (sa[i] + k) < N ? ra[sa[i] + k] : 0;
                int prev = (sa[i - 1] + k) < N ? ra[sa[i - 1] + k] : 0;
                if(cur == prev) {
                    tra[sa[i]] = r;
                }
                else {
                    tra[sa[i]] = ++r;
                }
            }
            else {
                tra[sa[i]] = ++r;
            }
        }
        if(tra[sa[N - 1]] == N - 1) {
            break;
        }
        memcpy(ra, tra, sizeof(int) * N);
    }
}

void build_lcp() {
    phi[sa[0]] = -1;
    fori(i, 1, N) {
        phi[sa[i]] = sa[i - 1];
    }
    int l = 0;
    fori(i, 0, N) {
        if(phi[i] == -1) {
            plcp[i] = 0;
            continue;
        }
        while(T[i + l] == T[phi[i] + l]) {
            l++;
        }
        plcp[i] = l;
        l = max(l - 1, 0);
    }
    fori(i, 0, N) {
        lcp[i] = plcp[sa[i]];
    }
}

