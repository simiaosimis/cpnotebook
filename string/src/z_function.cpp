// z[i] - length of the longest prefix of s that is also a prefix of s[i..n-1]
vector<int> z_function(const string &s) {
    int n = s.size();
    vector<int> z(n, 0);
    int l = 0, r = 0;
    fori(i, 1, n) {
        if(i <= r) {
            z[i] = min(z[i - l], r - i + 1);
        }
        while(z[i] + i < n && s[z[i] + i] == s[z[i]]) {
            z[i]++;
        }
        if(r < i + z[i] - 1) {
            l = i, r = i + z[i] - 1;
        }
    }
    return z;
}
