// p[] is 1-based, but s[] is 0-based
// p[i] - length of longest border of S[0..i-1]
vector<int> prefix_function(const string &s) {
    int n = s.size();
    vector<int> p(n + 1, 0);
    fori(i, 2, n + 1) {
        p[i] = p[i - 1];
        while(p[i] > 0 && s[p[i]] != s[i - 1]) {
            p[i] = p[p[i]];
        }
        if(s[p[i]] == s[i - 1]) {
            p[i]++;
        }
    }
    return p;
}
