const int MAX = 1e3 + 5; // maximum number of nodes
const int MAX_C = 31; // maximum number of alphabet

int aho[MAX][MAX_C], f[MAX];
bool match[MAX];
int NEXT_ID = 2; // node 1 is root

void add_word(char *ptr) {
    int node = 1;
    while(*ptr) {
        int mask = eval(*ptr);
        if(aho[node][mask] == -1) {
            aho[node][mask] = NEXT_ID++;
        }
        node = aho[node][mask];
        ptr++;
    }
    match[node] = true;
}

void build_failure() {
    memset(f, -1, sizeof f);
    f[1] = 1;
    queue<int> q;
    fori(i, 0, MAX_C) {
        int node = aho[1][i];
        if(node != -1) {
            f[node] = 1;
            q.push(node);
        }
    }
    while(!q.empty()) {
        int cur = q.front(); q.pop();
        fori(i, 0, MAX_C) {
            int nxt = aho[cur][i];
            if(nxt != -1) {
                int prev = f[cur];
                while(prev != 1 && aho[prev][i] == -1) {
                    prev = f[prev];
                }
                int guy = aho[prev][i] == -1 ? 1 : aho[prev][i];
                f[nxt] = guy;
                q.push(nxt);
            }
        }
    }
}

void search(const string &s) {
	int n = s.size();
	int node = 1;
	fori(i, 0, n) {
		int val = eval(s[i]);
		while(node > 1 && aho[node][val] == -1) {
			node = f[node];
		}
		node = aho[node][val] == -1 ? 1 : aho[node][val];
		int temp_node = node;
		while(!visited[temp_node]) {
			visited[temp_node] = true;
			if(match[temp_node]) {
				// do stuff
			}
			temp_node = f[temp_node];
		}
	}
}
