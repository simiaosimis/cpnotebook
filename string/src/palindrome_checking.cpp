const int MOD1 = 1e9 + 9;
const int MOD2 = 1e9 + 21;
const int A1 = 3137;
const int A2 = 3001;
const int MAX = 5e6 + 5;
const int v[2] = {A1, A2};
const int m[2] = {MOD1, MOD2};
int pa[2][MAX];
int n;
string S;
vector<ii> prefix_hashes, suffix_hashes;

inline int add(int a, int b, int M) {
    a += b;
    if(a >= M) {
        a -= M;
    }
    return a;
}

inline int mult(int a, int b, int M) {
    return (1LL * a * b) % M;
}

void prepare() {
    fori(i, 0, 2) {
        pa[i][0] = 1;
        fori(j, 1, MAX) {
            pa[i][j] = mult(pa[i][j - 1], v[i], m[i]);
        }
    }
}

vector<ii> ruben_hash(const string &s) {
    int size = s.size();
    int h[2]; h[0] = h[1] = 0;
    vector<ii> prefixes(size);
    fori(i, 0, 2) {
        fori(j, 0, size) {
            h[i] = add(h[i], mult(s[j], pa[i][j], m[i]), m[i]);
            if(i == 0) {
                prefixes[j].first = h[i];
            }
            else {
                prefixes[j].second = h[i];
            }
        }
    }
    return prefixes;
}

bool palindrome(int l, int r) {
    if(l == r) {
        return true;
    }
    int size = (r - l + 1);
    int L1 = l, R1, L2, R2 = r;
    if(size & 1) {
        R1 = l + size / 2 - 1;
        L2 = r - size / 2 + 1;
    }
    else {
        R1 = l + size / 2 - 1;
        L2 = l + size / 2;
    }
    ii fhash = ii((prefix_hashes[R1].first - (L1 == 0 ? 0 : prefix_hashes[L1 - 1].first)) % m[0], (prefix_hashes[R1].second - (L1 == 0 ? 0 : prefix_hashes[L1 - 1].second)) % m[1]);
    if(0 > fhash.first) {
        fhash.first += m[0];
    }
    if(0 > fhash.second) {
        fhash.second += m[1];
    }
    int sL2 = n - L2 - 1;
    int sR2 = n - R2 - 1;
    ii shash = ii((suffix_hashes[sL2].first - (sR2 == 0 ? 0 : suffix_hashes[sR2 - 1].first)) % m[0], (suffix_hashes[sL2].second - (sR2 == 0 ? 0 : suffix_hashes[sR2 - 1].second)) % m[1]);
    if(0 > shash.first) {
        shash.first += m[0];
    }
    if(0 > shash.second) {
        shash.second += m[1];
    }
    int fpot = L1;
    int spot = n - R2 - 1;
    if(spot > fpot) {
        fhash.first = mult(fhash.first, pa[0][spot - fpot], m[0]);
        fhash.second = mult(fhash.second, pa[1][spot - fpot], m[1]);
    }
    else {
        shash.first = mult(shash.first, pa[0][fpot - spot], m[0]);
        shash.second = mult(shash.second, pa[1][fpot - spot], m[1]);
    }
    return fhash == shash;
}

int degree[MAX];
char tmp[MAX];

int main() {
    prepare();

    scanf("%s", tmp);
    S = string(tmp);

    n = S.size();
    prefix_hashes = ruben_hash(S);

    string S_rev = S;
    reverse(S_rev.begin(), S_rev.end());
    suffix_hashes = ruben_hash(S_rev);

    return 0;
}

