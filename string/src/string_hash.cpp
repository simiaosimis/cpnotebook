#include <bits/stdc++.h>

#define debug(x) cout << #x << " = " << x << endl
#define fori(i, ini, lim) for(int i = int(ini); i < int(lim); i++)
#define ford(i, ini, lim) for(int i = int(ini); i >= int(lim); i--)

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;

const int MOD1 = 1e9 + 9;
const int MOD2 = 1e9 + 21;
const int A1 = 3137;
const int A2 = 3001;
const int MAX = 1e6 + 5;
const int v[2] = {A1, A2};
const int m[2] = {MOD1, MOD2};
int pa[2][MAX];

inline int add(int a, int b, int M) {
    a += b;
    if(a >= M) {
        a -= M;
    }
    return a;
}

inline int mult(int a, int b, int M) {
    return (1LL * a * b) % M;
}

void prepare() {
    fori(i, 0, 2) {
        pa[i][0] = 1;
        fori(j, 1, MAX) {
            pa[i][j] = mult(pa[i][j - 1], v[i], m[i]);
        }
    }
}

ii ruben_hash(const string &s) {
    int n = s.size();
    int h[2]; h[0] = h[1] = 0;
    fori(i, 0, 2) {
        fori(j, 0, n) {
            h[i] = add(h[i], mult(s[j], pa[i][j], m[i]), m[i]);
        }
    }
    return make_pair(h[0], h[1]);
}

int main() {
    return 0;
}